var args = arguments[0] || {};

//recibe la variable principal como argumento llamado parent
// funcion para establever la ventana principal drawer.setCenterWindow(newWin);

var win_myProfile = Alloy.createController('my_profile_win').getView();

var win_eco_ranking = Alloy.createController('eco_ranking_win').getView();
var win_ayudar_planeta = Alloy.createController('ayudar_planeta_win').getView();
var win_recordatiorios = Alloy.createController('recordatorios_win').getView();
var win_configuraciones = Alloy.createController('configuraciones_win').getView();

function open_win(){
	
	switch(this.id){
		case 'row1':
			Alloy.Globals.parent.setCenterWindow(win_myProfile);
			
		break;
		
		case 'row2':
		   var win_misEmisiones = Alloy.createController('mis_emisiones_win').getView();
			Alloy.Globals.parent.setCenterWindow(win_misEmisiones);
		break;
		
		case 'row3':
			Alloy.Globals.parent.setCenterWindow(win_eco_ranking);
		break;
		
		case 'row4':
			Alloy.Globals.parent.setCenterWindow(win_ayudar_planeta);
		break;
		
		case 'row5':
			Alloy.Globals.parent.setCenterWindow(win_recordatiorios);
		break;
		
		case 'row6':
			Alloy.Globals.parent.setCenterWindow(win_configuraciones);
		break;
		
	}
	
	Alloy.Globals.parent.toggleLeftWindow();
};


function log_out(){
	var aux = Alloy.createCollection('users');
		aux.fetch();
		
		while(aux.length) { 
		    aux.at(0).destroy(); 
		};
	
		Alloy.createController('index').getView().open();
		args.parent_window.close();
		
};
