var args = arguments[0] || {};

$.lbl_total.text = args.total;
$.lbl_porcentaje.text='Si todas las personas de México vivieran como tú, se necesitaría reforestar el '+args.porcentaje+'% del territorio Mexicano para contrarrestar tus emisiones de CO2';
$.img_background.width=Ti.Platform.displayCaps.plarformWidth;
$.img_background.height='auto';

 var fb = require('facebook');
 
  fb.addEventListener('shareCompleted', function (e) {
	        if (e.success) {
	            Ti.API.info('Share request succeeded.');
	        } else {
	            Ti.API.warn('Failed to share.');
	        }
	    });


   // var image = $.body_result.toImage(null, true);


	var _viewImage = Ti.UI.createView({
		height:585,
		width:640,
		backgroundColor:'white',
		backgroundImage:"/images/resultado_imagen.png",
		layout:'vertical'
	});
	
	_viewImage.add(Ti.UI.createLabel({
		width:580,
		text:'El resultado de mi contaminacion:',
		textAlign:'center',
		height:'auto',
		font:{fontSize:28},
		top:80,
		color:'black'
	}));
	
	_viewImage.add(Ti.UI.createLabel({
		width:580,
		text:args.total,
		textAlign:'center',
		height:'auto',
		font:{fontSize:34, fontWeight:'bold'},
		top:10,
		color:'red'
	}));
	
	_viewImage.add(Ti.UI.createLabel({
		width:580,
		text:"Si todas las personas de México vivieran como tú, se necesitaría reforestar el "+args.porcentaje+"% del territorio Mexicano para contrarrestar tus emisiones de CO2",
		textAlign:'center',
		height:'auto',
		font:{fontSize:18},
		top:10,
		color:'black'
	}));
	
    var image = _viewImage.toImage(null, true);

	//Saving the image if it is needed somewhere else
	var f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, "finalChallengeImage.jpeg");
	f.write(image);
	
	Ti.Media.saveToPhotoGallery(image,{
        success: function(e){
            console.log('Saved image to gallery');
        },
        error: function(e){
            console.log("Error trying to save the image.");
        }
    });

function share_fb(){
	   
	    var publishObj = {
	        link: 'http://co2labora.coffeebit.us',
	        name: '¿Y tú cuanto contaminas?',
	        description: 'Co2labora una propuesta de Coffeebit',
	        caption: 'La mejor manera de ayudar al mundo y tomar conciencia en la palma de tu mano',
	        picture: 'http://www.kinergy.mx/result.jpg'
	    };
	    
    if(fb.getCanPresentShareDialog()) {
        //Ti.API.info('Can Present Share Dialog');
        fb.presentShareDialog(publishObj);
    } else {
        //Ti.API.info('Can NOT Present Share Dialog - Publishing Web Dialog');
        fb.presentWebShareDialog(publishObj);
    }
};
