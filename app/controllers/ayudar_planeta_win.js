var args = arguments[0] || {};

var ANCHO_IMG = Ti.Platform.displayCaps.platformWidth;

function event_scroll(e){
	
	if(e.y<0){
		
		$.img_front.visible = false;
		
		var height = ANCHO_IMG - e.y;
		var scale = height / ANCHO_IMG;
		
		var transform = Ti.UI.create2DMatrix({scale: scale});
		transform = transform.translate(0, -e.y/(2*scale));
		
		$.img_back.transform = transform;
		
	}
	else
	  $.img_front.visible = true;
};