var args = arguments[0] || {};
var fb = require('facebook');

$.main_window.open();

//args.total_co2
//args.total_arboles


//:::::::::Slide menu module ::::::::

var NappDrawerModule = require('dk.napp.drawer');

function createMenuWindow(){
	var win = Ti.UI.createWindow({
		statusBarStyle:Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT
	});
	
	var menu_view = Alloy.createController('menu',{parent_window:$.main_window}).getView();
	win.add(menu_view);
	
	return win;
}


function createCenterNavWindow(){	
	var leftBtn = Ti.UI.createButton({
		height:35,
		width:35,
		backgroundImage:'/images/menuicon.png',
	});
	leftBtn.addEventListener("click", function(){
		drawer.toggleLeftWindow();
	});

	
	var win = Ti.UI.createWindow({
		backgroundColor:'white',
		translucent:false,
		title:"NappDrawer",
		barColor:"#2ECC71",
		tintColor:"white",
		leftNavButton: leftBtn,
		titleControl:Ti.UI.createLabel({ text: 'Mis emisiones', color: 'white', width: Ti.UI.SIZE}),
		statusBarStyle:Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT
	});
	
	//Virifica si tiene emisiones 
	
	//Toma el ultimo  valor almacenado den el modelo 
	var aux = Alloy.createCollection('emisiones');
		aux.fetch();
	//console.log('Total de emisiones: '+aux.length);
	
	
		//console.log('utlimo resultado: '+aux.fetch(0)); 
	if(aux.length>0){
		var recoverDatabase = Alloy.createCollection("emisiones");     
        recoverDatabase.fetch({query:"SELECT * FROM emisiones WHERE id="+aux.length});
        var _a=recoverDatabase.at(0).get("total");
        var _b = recoverDatabase.at(0).get("porcentaje");
        
    
        console.log("Total: "+_a+" porcentaje: "+_b);
		
		
		var label = Ti.UI.createLabel({
			text:'El total de tus emisiones actuales es:',
			width:'100%',
			textAlign:'center',
			font:{fontSize:18},
			top:15,
			color:'black'
		});
		
		win.add(label);
		
		label = Ti.UI.createLabel({
			font:{fontSize:20, fontWeight:'bold'},
			color:'red',
			top:45,
			width:'100%',
			text:_a,
			textAlign:'center'
		});
		win.add(label);
		
		var comentarios_view = Ti.UI.createView({
			height:Ti.UI.SIZE,
			width:'100%',
			layout:'vertical',
			top:100
		});
		
		label = Ti.UI.createLabel({
			text:'Si todas las personas de México vivieran como tú, se necesitaría reforestar el '+_b+'% del territorio Mexicano para contrarrestar tus emisiones de CO2',
			color:'black',
			font:{fontSize:18},
			textAlign:'center',
			width:'90%',
			top:100
		});
		
		win.add(label);
		
	
		
		var img = Ti.UI.createImageView({
			height:'auto',
			width:Ti.Platform.displayCaps.plarformWidth,
			image:'/images/resultado_imagen.png',
			bottom:0
		});
		win.add(img);
		
		var share = Ti.UI.createImageView({
			height:40,
			width:'auto',
			image:'/images/resultado_compartir.png',
			zIndex:10,
			bottom:75
		});
		
		share.addEventListener('click', share_fb);
		
		
		win.add(share);
		
		var view_tip1 = Ti.UI.createView({
			backgroundColor:'#2ECC71',
			width:Ti.Platform.displayCaps.platformWidth,
			left:Ti.Platform.displayCaps.platformWidth*-1,
			
		});
		
		view_tip1.add(Ti.UI.createImageView({
			left:0,
			height:60,
			width:60,
			image:"/images/image_tip.png"
		}));
		
		var close_img = Ti.UI.createImageView({
			height:22,
			width:22,
			image:'/images/close_tip.png',
			top:5,
			right:5
		});
		
		close_img.addEventListener('click', function(){
			scroll_tips.scrollToView(1);
		});
		
		view_tip1.add(close_img);
		
		view_tip1.add(Ti.UI.createLabel({
			text:'¿Sabias que?',
			font:{fontSize:16, fontWeight:'bold'},
			color:'white',
			left:70,
			top:5
		}));
		
		view_tip1.add(Ti.UI.createLabel({
			text:'Un foco de 100 Watts encendido por una hora produce un costo de $10 pesos',
			color:'white',
			top:25,
			font:{fontSize:12},
			left:70,
			width:"80%"
		}));
		
		setTimeout(function() {
			
			view_tip1.animate({
				left:0,
				duration:1000
			});
			
		}, 1000);
		
		
		var view_tip2 = Ti.UI.createView({});
		
		
		var scroll_tips = Ti.UI.createScrollableView({
			height:60,
			width:'100%',
			views:[view_tip1, view_tip2],
			zIndex:11,
			bottom:0
		});
		
		win.add(scroll_tips);
	}
	else{
			var _sinEvaluaciones_view = Alloy.createController("sin_evaluacion").getView();
			_sinEvaluaciones_view.height='100%';
			win.add(_sinEvaluaciones_view);
	}
		
	
	var navController =  Ti.UI.iOS.createNavigationWindow({
		window : win
	});
	
	
	return navController;
}

var mainWindow = createCenterNavWindow();

//Alloy.Globals.main_Window=mainWindow;
	

var drawer = NappDrawerModule.createDrawer({
	leftWindow: createMenuWindow(),
	centerWindow: mainWindow,
	closeDrawerGestureMode: NappDrawerModule.CLOSE_MODE_ALL,
	openDrawerGestureMode: NappDrawerModule.OPEN_MODE_ALL,
	showShadow: false, 
	leftDrawerWidth: 270,
	rightDrawerWidth: 120,
	statusBarStyle: NappDrawerModule.STATUSBAR_WHITE,  // remember to set UIViewControllerBasedStatusBarAppearance to false in tiapp.xml
	orientationModes: [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT],
	animationMode:NappDrawerModule.ANIMATION_SLIDE_SCALE
});

Alloy.Globals.parent = drawer;

drawer.addEventListener('windowDidOpen', function(e) {
	Ti.API.info("windowDidOpen");
});

drawer.addEventListener('windowDidClose', function(e) {
	Ti.API.info("windowDidClose");
});

drawer.open();

Ti.API.info("isAnyWindowOpen: " + drawer.isAnyWindowOpen());

function share_fb(){
	   
	    var publishObj = {
	        link: 'http://co2labora.coffeebit.us',
	        name: '¿Y tú cuanto contaminas?',
	        description: 'Co2labora una propuesta de Coffeebit',
	        caption: 'La mejor manera de ayudar al mundo y tomar conciencia en la palma de tu mano',
	        picture: 'http://www.kinergy.mx/result.jpg'
	    };
	    
    if(fb.getCanPresentShareDialog()) {
        //Ti.API.info('Can Present Share Dialog');
        fb.presentShareDialog(publishObj);
    } else {
        //Ti.API.info('Can NOT Present Share Dialog - Publishing Web Dialog');
        fb.presentWebShareDialog(publishObj);
    }
};