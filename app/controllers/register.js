var args = arguments[0] || {};

function register(){
	console.log($.name.mainText.value);
	
	var _name = $.name.mainText.value;
	var _email = $.mail.mainText.value;
	var _pass = $.pass.mainText.value;
	var _repass = $.repass.mainText.value;
	
	if(_pass==_repass){
		var xhr = Ti.Network.createHTTPClient({
			onload:function(){
				var json = JSON.parse(this.responseText);
				console.log(json);
				
				var msg = Ti.UI.createAlertDialog({
					title:'Registro exitoso',
					message:'El usuarios se ha registrado exitosamente',
					ok:'Aceptar',
				});
				
				msg.addEventListener('click', function(){
					var page = Alloy.createController('main_form',{parent:args.parent}).getView();
					args.parent.openWindow(page);
				});
				
				msg.show();
				
			},
			onerror:function(e){
				console.error('Error en la API de registro: '+ e.error);
			},
			ondatastream:function(){
				console.log(':::::Registering API::::');
			}
		});
		
		xhr.open('POST', 'http://www.coffeebit.us:3030/signUp');
		xhr.send({
			email : _email,
	        password : _pass,
	        userFb : 0, //0 false, 1 true
	        username : "no", //optional
	        idFb: "no", //optional
	        confirmate : 1, //0 false, 1 true
		});
	}
	else{
		var msg = Ti.UI.createAlertDialog({
			title:'Verificar contraseña',
			message:'Las contraseñas no coinciden, favor de verificarlas',
			ok:'Aceptar'
		});
		
		msg.addEventListener('click', function(){
			$.pass.mainText.value='';
			$.repass.mainText.value="";
		});
		
		msg.show();
	}
	
	
	
	



};
