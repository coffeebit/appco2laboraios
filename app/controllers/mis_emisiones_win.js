var args = arguments[0] || {};

//var _userlogued = Alloy.createCollection('users');
//	_userlogued.fetch();

var library = Alloy.Collections.emisiones;
library.fetch();

console.log('Total de registros: '+ library.length);

if(library.length>0){
	console.log('------>Si hay registros');
	$.sin_evaluacion.removeAllChildren();
	$.sin_evaluacion.height=0;
};
/*else{
	$.sin_evaluacion.visible=true;
	console.log('------>No hay registros');
	
};
	*/

function hidemenu(){
	Alloy.Globals.parent.toggleLeftWindow();
};


function do_form(){
	
	var nav_win = Titanium.UI.iOS.createNavigationWindow({});
	
	var _form = Alloy.createController('main_form',{mis_emisiones_win:true, nav_parent:nav_win}).getView();
	
	  
	 nav_win.window=_form;
	 
	 nav_win.open({modal:true});
	
};

function show_win_details (_total, _porcentaje){
	 var win = Ti.UI.createWindow({
	 	backgroundColor:'white',
	 	layout:'vertical',
	 	statusBarStyle:Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT
	 });
	 
	 //Obtinee el header de la ventana
	 var _header = Alloy.createController('header_navbar', {title:'Mis emisiones'}).getView();
	 
	 //Elimina la parte del menu 
	 _header.children[0].visible=false;
	 
	 //Crea el icono para cerrar 
	 var _close = Ti.UI.createView({
	 	height:'100%',
	 	width:50,
	 	right:0
	 });
	 
	 _close.add(Ti.UI.createImageView({
	 	height:35,
	 	width:35,
	 	image:'/images/close.png'
	 }));
	 
	 _close.addEventListener('click', function(){
	 	win.close();
	 });
	 
	 _header.add(_close);
	 
	 win.add(_header);
	 
	 var _body = Alloy.createController('body_result',{total:_total,porcentaje:_porcentaje}).getView();
	 
	 win.add(_body);
	 
	 return win;
	 
};


function open_details(e){
	// Get the section of the clicked item
	var section = $.main_list.sections[e.sectionIndex];
	// Get the clicked item from that section
	var item = section.getItemAt(e.itemIndex);

	
	//console.log(item.properties.porcentaje);
	//console.log(item.properties.date);
	//console.log(item.properties.co);
	var _win = show_win_details(item.properties.co, item.properties.porcentaje);
	_win.open({modal:true});
	
};


