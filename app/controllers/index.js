var fb = require('facebook');

var page;

//Verifica si el usuario ya está logueado 
var _userlogued = Alloy.createCollection('users');
	_userlogued.fetch();
	
	console.log('Usuarios logueados: '+_userlogued.length);
	if(_userlogued.length>0)
		Alloy.createController('main_window').getView().open();
	else
		$.win_nav.open();
		
	


function openRegister(e) {
   page = Alloy.createController('register',{parent:$.win_nav}).getView();
   $.win_nav.openWindow(page);
};

function login(){
	
	if($.txt_username.value!='' && $.txt_password.value!=''){
		
		var xhr = Ti.Network.createHTTPClient({
			onload:function(){
				var json = JSON.parse(this.responseText);
				console.log(json);
				console.log('Login Exitoso');
				
				//console.log(json.data.error);
				if(json.data.error==0){
					
					var _user = Alloy.createModel('users');
		            _user.set({
		            	id:json.data.user[0].id,
		            	email:json.data.user[0].email
		            });
		            _user.save(); 
										
					page = Alloy.createController('main_form',{parent:$.win_nav}).getView();
					$.win_nav.openWindow(page);
					
				
					//console.log(json.data.user[0].id);
					
					
					
				}
				else{
					var msg = Ti.UI.createAlertDialog({
						title:'Error en login',
						message:'Usuario o contraseña invalidos',
						ok:'Aceptar'
					});
					
					msg.addEventListener('click', function(){
						$.txt_password.value="";
					});
					
					msg.show();
				}
				
				
			},
			onerror:function(e){
				console.error('Error en la API login: '+e.error);
			},
			ondatastream:function(){
				console.log(':::::Login API::::::');
			}
		});
		
		xhr.open('POST', 'http://www.coffeebit.us:3030/signIn');
		xhr.send({
			email : $.txt_username.value,
	        password : $.txt_password.value,
		});
	}
	else{
		var msg = Ti.UI.createAlertDialog({
			title:'Error en login',
			message:'Favor de llenar todos los campos',
			ok:'Aceptar'
		});
		
		msg.addEventListener('click', function(){
			//$.txt_username.value='';
			$.txt_password.value='';
		});
		
		msg.show();
	}
	
	
	
	
};


fb.addEventListener('login',function(e) {
		// You *will* get this event if loggedIn == false below
     	// Make sure to handle all possible cases of this event
     	if (e.success) {
 			console.log('login from uid: '+e.uid+', name: '+JSON.parse(e.data).name);
 			console.log('Logged In = ' + fb.loggedIn);
 			page = Alloy.createController('main_form',{parent:$.win_nav}).getView();
			$.win_nav.openWindow(page);
     	}
     	else if (e.cancelled) {
       		// user cancelled 
       		console.log('cancelled');
     	}
     	else {
       		console.log(e.error);   		
     	}
  	});
  	
fb.addEventListener('logout', function(e) {
	console.log('logged out');
	console.log('Logged In = ' + fb.loggedIn);
});
	
	

	
function fb_login(){
	var loginButton = fb.createLoginButton({
		readPermissions: ['read_stream','email']
	});
	
	loginButton.readPermissions = ['email'];
	
	if (!fb.loggedIn) {
	// then you want to show a login UI
		fb.authorize();
	}
	
};



//$.win_nav.open();

