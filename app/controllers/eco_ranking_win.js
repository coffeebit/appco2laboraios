var args = arguments[0] || {};

function hidemenu(){
	Alloy.Globals.parent.toggleLeftWindow();
};

var new_item;
for (var i = 0; i < 6; i++){
	
	switch(i){
		case 0:
			new_item = Alloy.createController('item_ecoRanking',{name:'Nestor Cardenas', cantidad:'0.5', premio:'/images/ranking_oro.png'}).getView();
		break;
		
		case 1:
			new_item = Alloy.createController('item_ecoRanking',{name:'Cesar Buenrostro', cantidad:'1.7', premio:'/images/ranking_plata.png'}).getView();
		break;
		
		case 2:
			new_item = Alloy.createController('item_ecoRanking',{name:'Carlos Partida', cantidad:'2', premio:'/images/ranking_bronce.png'}).getView();
		break;
		
		default:
			new_item = Alloy.createController('item_ecoRanking',{name:'Alexis Maturano', cantidad:'2.5', premio:''}).getView();
		
	};
	
	
	$.main_scroll.add(new_item);
};
