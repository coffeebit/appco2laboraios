var args = arguments[0] || {};


function edit(){
	$.edit_view.visible=true;
	
	$.edit_view.animate({
		opacity:1,
		duration:600
	});
};

function cancel(){
	$.edit_view.animate({
		opacity:0,
		duration:600
	});
	
	
	setTimeout(function() {
		$.edit_view.visible = false;
	}, 700);
};


function hidemenu(){
	Alloy.Globals.parent.toggleLeftWindow();
};
