var args = arguments[0] || {};

 var fb = require('facebook');

var emptyView = Titanium.UI.createView({});
$.main_form.leftNavButton = emptyView;

var right_button = Ti.UI.createLabel({
	text:'Omitir',
	color:'white'
});

right_button.addEventListener('click', function(){
	
	
	if(args.mis_emisiones_win){  // si la peticion es enviada desde el contralador "mi_emisiones"
		args.nav_parent.close();
	}
	else{ //Si la peticion es enviada al iniciar
		page = Alloy.createController('main_window').getView();
		args.parent.openWindow(page);
	}
});

$.main_form.rightNavButton = right_button;


//Calcula las emisiones mediante formulas
/*
 					gasCO2=3*gas;
                    autoCO2=2.3*(gasolina*4);
                    luzCO2=.5827*(Kwh/2);

                    totalCO2=gasCO2+autoCO2+luzCO2+miBasuraCO2;// Emiciones totales al mes

                    hectarias=((118395054*(totalCO2*12))/1000)/6;
                    porcentajeArboles=(hectarias*100)/195924798; //saber cuanto porcentaje cubren las hectarias necesarias en el territorio mexicano

                    Intent intent = new Intent(getActivity(), ResultActivity.class);
                    intent.putExtra("CO2",totalCO2);
                    intent.putExtra("porcentaje",porcentajeArboles);
 */

var TOTAL_CO2 = {
	luz:0,
	gasolina:0,
	gas:0,
	basura:0
};

var calcular_co2=({
	calculaLuz: function(Kwh){
		//console.log('valor:'+valor);
		 TOTAL_CO2.luz=((Kwh/2)*0.5827);
		 //console.log('El valor de CO2 de la Luz:'+((Kwh/2)*0.5827));
	},
	
	calculaGasolina:function(litros){
		TOTAL_CO2.gasolina=(2.3*litros*4);
		//console.log('El resultado de CO2 de gasolina es: '+(2.3*litros*4));
	},
	
	calculaGas: function(kg){
		TOTAL_CO2.gas =(kg*3);
		//console.log('El resultado de CO2 de Gas es: '+ (kg*3));
	},
	caculaBasura:function(porcentaje){
		TOTAL_CO2.basura = ((porcentaje*30.38)/100);
		//console.log('El resultado de CO2 de Basura es: '+((porcentaje*30.38)/100));
	}

});

		
var page;
function change_ligth(e){
	//console.log(String.format("%3.1f", e.value));
	$.total_luz.text = String.format("%2.0f", e.value);
	
	calcular_co2.calculaLuz(e.value);
	
	
		if(e.value==0)
			$.img_luz.image='/images/luz/item1.png';
		else if( e.value > 0 && e.value<200)
			$.img_luz.image='/images/luz/item2.png';
		else if(e.value<400)
			$.img_luz.image='/images/luz/item3.png';
		else if(e.value<600)
			$.img_luz.image='/images/luz/item4.png';
		else if(e.value<800)
			$.img_luz.image='/images/luz/item5.png';
		else if(e.value<1000)
			$.img_luz.image='/images/luz/item6.png';
		else if(e.value<1200)
			$.img_luz.image='/images/luz/item7.png';
		else if(e.value<1400)
			$.img_luz.image='/images/luz/item8.png';
		else if(e.value<1600)
			$.img_luz.image='/images/luz/item9.png';
		else if(e.value<1800)
			$.img_luz.image='/images/luz/item10.png';
		else if(e.value<=2000)
			$.img_luz.image='/images/luz/item11.png';
		
};

function change_gasoline(e){
	//console.log(String.format("%3.1f", e.value));
	$.total_gasoline.text = String.format("%2.0f", e.value);
	calcular_co2.calculaGasolina(e.value);
	
	if(e.value<20)
			$.img_auto.image='/images/gasoline/item1.png';
		else if( e.value<40)
			$.img_auto.image='/images/gasoline/item2.png';
		else if(e.value<60)
			$.img_auto.image='/images/gasoline/item3.png';
		else if(e.value<80)
			$.img_auto.image='/images/gasoline/item4.png';
		else if(e.value<100)
			$.img_auto.image='/images/gasoline/item5.png';
		else if(e.value<120)
			$.img_auto.image='/images/gasoline/item6.png';
		else if(e.value<140)
			$.img_auto.image='/images/gasoline/item7.png';
		else if(e.value<160)
			$.img_auto.image='/images/gasoline/item8.png';
		else if(e.value<180)
			$.img_auto.image='/images/gasoline/item9.png';
		else if(e.value<=200)
			$.img_auto.image='/images/gasoline/item10.png';
		
};

function change_gas(e){
	//console.log(String.format("%3.1f", e.value));
	$.total_gas.text = String.format("%2.0f", e.value);
	calcular_co2.calculaGas(e.value);
	
	if(e.value<10)
			$.img_gas.image='/images/gas/item1.png';
		else if( e.value<20)
			$.img_gas.image='/images/gas/item2.png';
		else if(e.value<30)
			$.img_gas.image='/images/gas/item3.png';
		else if(e.value<40)
			$.img_gas.image='/images/gas/item4.png';
		else if(e.value<50)
			$.img_gas.image='/images/gas/item5.png';
		else if(e.value<60)
			$.img_gas.image='/images/gas/item6.png';
		else if(e.value<70)
			$.img_gas.image='/images/gas/item7.png';
		else if(e.value<80)
			$.img_gas.image='/images/gas/item8.png';
		else if(e.value<90)
			$.img_gas.image='/images/gas/item9.png';
		else if(e.value<=100)
			$.img_gas.image='/images/gas/item10.png';
};

function change_trash(e){
	//console.log(String.format("%3.1f", e.value));
	$.total_trash.text = String.format("%2.0f", e.value)+"%";
	calcular_co2.caculaBasura(e.value);
	
	if(e.value<10)
			$.img_trash.image='/images/trash/item1.png';
		else if( e.value<20)
			$.img_trash.image='/images/trash/item2.png';
		else if(e.value<30)
			$.img_trash.image='/images/trash/item3.png';
		else if(e.value<40)
			$.img_trash.image='/images/trash/item4.png';
		else if(e.value<50)
			$.img_trash.image='/images/trash/item5.png';
		else if(e.value<60)
			$.img_trash.image='/images/trash/item6.png';
		else if(e.value<70)
			$.img_trash.image='/images/trash/item7.png';
		else if(e.value<80)
			$.img_trash.image='/images/trash/item8.png';
		else if(e.value<90)
			$.img_trash.image='/images/trash/item9.png';
		else if(e.value<=100)
			$.img_trash.image='/images/trash/item10.png';
	
};

function gomainwin(e){
	var TOTAL = (TOTAL_CO2.luz+TOTAL_CO2.gasolina+TOTAL_CO2.gas+TOTAL_CO2.basura);
	
	var _hectarias=((118395054*(TOTAL*12))/1000)/6;
	
    var _porcentajeArboles = ((_hectarias/100)*100)/1959247.98;
    
    //console.log('Resultado total de CO2: '+(TOTAL_CO2.luz+TOTAL_CO2.gasolina+TOTAL_CO2.gas+TOTAL_CO2.basura)/1000);
    //console.log('_hectareas: '+ _hectarias);
    ///console.log('_porcentajeArboles: '+ _porcentajeArboles);
    console.log('Luz: '+TOTAL_CO2.luz);
    console.log("Gasolina: "+ TOTAL_CO2.gasolina);
    console.log("Gas: "+ TOTAL_CO2.gas);
    console.log("Basura: "+ TOTAL_CO2.basura);
    
    console.log("\nTOTAL CO2: "+ String.format("%2.1f",(TOTAL/1000)));
    
    saveResult(String.format("%2.1f",(TOTAL/1000)), String.format("%3.1f", _porcentajeArboles));
    
	if(args.mis_emisiones_win){
		
		var result = createResultWindow((TOTAL/1000), _porcentajeArboles);
		args.nav_parent.close();
		Alloy.Globals.parent.setCenterWindow(result);
	}
	else{
		page = Alloy.createController('main_window',{total_co2:String.format("%2.1f",(TOTAL/1000)), total_arboles:String.format("%3.1f", _porcentajeArboles)}).getView();
		args.parent.openWindow(page);
	};
	
	
};

//test


function createResultWindow(Total_Co2, porcentaje_arboles){
	var _navbar = Alloy.createController('header_navbar',{title:'Mis emisiones'}).getView();
	
	var _win = Ti.UI.createWindow({
		layout:'vertical',
		backgroundColor:'white',
		statusBarStyle:Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT
	});
	
	_win.add(_navbar);
	
	var _viewContent= Ti.UI.createView();
	
	var label = Ti.UI.createLabel({
		text:'El total de tus emisiones es:',
		width:'100%',
		textAlign:'center',
		font:{fontSize:18},
		top:15,
		color:'black'
	});
	
	_viewContent.add(label);
	
	label = Ti.UI.createLabel({
		font:{fontSize:20, fontWeight:'bold'},
		color:'red',
		top:45,
		width:'100%',
		text:String.format("%2.1f", Total_Co2)+' Toneladas de CO2',
		textAlign:'center'
	});
	_viewContent.add(label);
	
	var comentarios_view = Ti.UI.createView({
		height:Ti.UI.SIZE,
		width:'100%',
		layout:'vertical',
		top:100
	});
	
	label = Ti.UI.createLabel({
		text:'Si todas las personas de México vivieran como tú, se necesitaría reforestar el '+String.format("%3.1f", porcentaje_arboles)+'% del territorio Mexicano para contrarrestar tus emisiones de CO2',
		color:'black',
		font:{fontSize:18},
		textAlign:'center',
		width:'90%'
	});
	
	comentarios_view.add(label);
		
	
	_viewContent.add(comentarios_view);
	
	var img = Ti.UI.createImageView({
		height:'auto',
		width:Ti.Platform.displayCaps.plarformWidth,
		image:'/images/resultado_imagen.png',
		bottom:0
	});
	_viewContent.add(img);
	
	var share = Ti.UI.createImageView({
		height:40,
		width:'auto',
		image:'/images/resultado_compartir.png',
		zIndex:10,
		bottom:75
	});
	
	share.addEventListener('click', share_fb);
	
	_viewContent.add(share);
	
	var view_tip1 = Ti.UI.createView({
		backgroundColor:'#2ECC71',
		width:Ti.Platform.displayCaps.platformWidth,
		left:Ti.Platform.displayCaps.platformWidth*-1,
		
	});
	
	view_tip1.add(Ti.UI.createImageView({
		left:0,
		height:60,
		width:60,
		image:"/images/image_tip.png"
	}));
	
	var close_img = Ti.UI.createImageView({
		height:22,
		width:22,
		image:'/images/close_tip.png',
		top:5,
		right:5
	});
	
	close_img.addEventListener('click', function(){
		scroll_tips.scrollToView(1);
	});
	
	view_tip1.add(close_img);
	
	view_tip1.add(Ti.UI.createLabel({
		text:'¿Sabias que?',
		font:{fontSize:16, fontWeight:'bold'},
		color:'white',
		left:70,
		top:5
	}));
	
	view_tip1.add(Ti.UI.createLabel({
		text:'Un foco de 100 Watts encendido por una hora produce un costo de $10 pesos',
		color:'white',
		top:25,
		font:{fontSize:12},
		left:70,
		width:"80%"
	}));
	
	setTimeout(function() {
		
		view_tip1.animate({
			left:0,
			duration:1000
		});
		
	}, 1000);
	
	
	var view_tip2 = Ti.UI.createView({});
	
	
	var scroll_tips = Ti.UI.createScrollableView({
		height:60,
		width:'100%',
		views:[view_tip1, view_tip2],
		zIndex:11,
		bottom:0
	});
	
	_viewContent.add(scroll_tips);
	
	_win.add(_viewContent);
	
	return _win;
	
};


function saveResult(totalx, porcentajex){

	
	var _data = Alloy.createModel('emisiones');
        _data.set({
        	total:totalx +' Ton de CO2',
        	porcentaje:porcentajex,
        	fecha:getDate()
        });
        _data.save(); 
    
	
};


function getDate (){
	var date = new Date();
	
	var _dia = date.getDate();
	var _mes = date.getMonth();
	var _anio = date.getFullYear();
	
	var _fecha = '';

	switch(_mes){
		case 0:
		_fecha = _dia+' de Enero del '+_anio;
		break;
		
		case 1:
		_fecha = _dia+' de Febrero del '+_anio;
		break;
		
		case 2:
		_fecha = _dia+' de Marzo del '+_anio;
		break;
		
		case 3:
		_fecha = _dia+' de Abril del '+_anio;
		break;
		
		case 4:
		_fecha = _dia+' de Mayo del '+_anio;
		break;
		
		case 5:
		_fecha = _dia+' de Junio del '+_anio;
		break;
		
		case 6:
		_fecha = _dia+' de Julio del '+_anio;
		break;
		
		case 7:
		_fecha = _dia+' de Agosto del '+_anio;
		break;
		
		case 8:
		_fecha = _dia+' de Septiembre del '+_anio;
		break;
		
		case 9:
		_fecha = _dia+' de Octubre del '+_anio;
		break;
		
		case 10:
		_fecha = _dia+' de Noviembre del '+_anio;
		break;
		
		case 11:
		_fecha = _dia+' de Diciembre del '+_anio;
		break;
	}	
	
	return _fecha;
	
};

function share_fb(){
	   
	    var publishObj = {
	        link: 'http://co2labora.coffeebit.us',
	        name: '¿Y tú cuanto contaminas?',
	        description: 'Co2labora una propuesta de Coffeebit',
	        caption: 'La mejor manera de ayudar al mundo y tomar conciencia en la palma de tu mano',
	        picture: 'http://www.kinergy.mx/result.jpg'
	    };
	    
    if(fb.getCanPresentShareDialog()) {
        //Ti.API.info('Can Present Share Dialog');
        fb.presentShareDialog(publishObj);
    } else {
        //Ti.API.info('Can NOT Present Share Dialog - Publishing Web Dialog');
        fb.presentWebShareDialog(publishObj);
    }
};



